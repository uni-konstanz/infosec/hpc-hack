#!/bin/bash

###
#
#  This script is used to check if malicious ip addresses exist
#  on your Linux system:
#
#     $ sudo ./ssh-sec_check-ip-addresses.sh
#
#  Moreover this script parses netstat table for each listed 
#  IP address.  
#
#  see <https://csirt.egi.eu/academic-data-centers-abused-for-crypto-currency-mining/>
#
# 
# Authors: yannick.leist@uni-konstanz.de
#          markus.grandpre@uni-konstanz.de
#          daniel.scharon@uni-konstanz.de
#
# Version: $Id: ssh-sec_check-ip-addresses.sh 1692 2020-05-26 14:44:49Z yannick.leist $
#

#
# Array of all IP addresses
#
IP_ADDRESSES=(
91.196.70.109
149.156.26.227
149.156.26.56
142.150.255.49
159.226.234.29
51.77.135.89
51.15.177.65
51.75.52.118
51.75.144.43
51.79.53.139
51.79.86.181
212.83.166.62
159.226.88.110
159.226.62.107
159.226.170.127
132.230.222.12
192.154.2.203
129.49.37.67
129.49.170.118
149.156.26.227
202.120.32.231
202.120.58.243
202.120.58.244
2001:da8:8000:6300::/64
159.226.161.107
159.226.234.29);

echo "Checking /var/log/ for suspicious IP adresses"

#
# Iterate IP address array
#
for IP_ADDRESS in "${IP_ADDRESSES[@]}"
do
   # check ip adresss
   echo "Checking $IP_ADDRESS"

   # search for ip address in 
   # every (compressed) file
   grep -r "$IP_ADDRESS" /var/log/
   find /var/log -type f -name "*.gz" -exec zgrep "$IP_ADDRESS" {} \;

   # parse netstat table
   ss | grep "$IP_ADDRESS"
done;

exit 0;
