#!/bin/sh
# author: daniel.scharon

for ip in 91.196.70.109	149.156.26.227 149.156.26.56 142.150.255.49 159.226.234.29 51.77.135.89 51.15.177.65 51.75.52.118 51.75.144.43 51.79.53.139 51.79.86.181 212.83.166.62 159.226.88.110 159.226.62.107 159.226.170.127 132.230.222.12 192.154.2.203 129.49.37.67 129.49.170.118 202.120.32.231 202.120.58.243	202.120.58.244 159.226.161.107
do
    grep -r "$ip" /var/log/*
    find /var/log -name "*.gz" -exec zgrep "$ip" {} \;
    ss |grep "$ip"
done

kill -63 "$(ps -e |tail -1 |cut -d" " -f1)"
lsmod |egrep '(diamorphine|scsi|iscsi|readaps)'

if [ -f /etc/cron.hourly/0anacron ]
then
  echo "contents of /etc/cron.hourly/0anacron:"
  cat /etc/cron.hourly/0anacron
else
  echo 'cannot find /etc/cron.hourly/0anacron'
fi

ls -hl /home/*/.mozilla/xdm
ls -hl $HOME/.mozilla/xdm
ls -hl /tmp/.dbs*
ls -hl /tmp/.lock
ls -hl /tmp/aes.tgz
ls -hl /tmp/db.tgz
ls -hl /tmp/dbsyn*
ls -hl /tmp/reserved
ls -hl /tmp/systemdb
ls -hl /tmp/updatedb
ls -hl /tmp/check_power
ls -hl /tmp/hdshare
ls -hl /tmp/readps
ls -hl /usr/bin/on_ac_power
ls -hl /usr/lib/libocs.so
ls -hl /usr/lib64/.lib/l64
ls -hl /usr/share/aldi.so
ls -hl /usr/share/sos/rh.pub
ls -hl /var/tmp/.lock
ls -hl /var/tmp/.lock/clogs
ls -hl /var/tmp/.lock/cpa.h
ls -hl /var/tmp/.lock/ologs
ls -hl /wlcg/arc-ce1/cache/.cache
ls -hl /apps/.ior/read/.terma
ls -hl /apps/.ior/read/.termb
ls -hl /etc/fonts/.fonts
ls -hl /etc/fonts/.low
ls -hl /etc/terminfo/.terma
ls -hl /etc/terminfo/.termb
ls -hl $HOME/.mozilla/plugins/.fonts
ls -hl $HOME/.mozilla/plugins/.low
ls -hl $HOME/.mozilla/plugins/.aa
ls -hl $HOME/.mozilla/plugins/test
ls -hl /home/*/.mozilla/plugins/.fonts
ls -hl /home/*/.mozilla/plugins/.low
ls -hl /home/*/.mozilla/plugins/.aa
ls -hl /home/*/.mozilla/plugins/test
ls -hl /usr/lib64/.lib/l64
ls -hl /var/games/.terma
ls -hl /var/games/.termb

echo 'le fin'

exit 0
