#!/bin/bash

###
#
#  This script is used to check if signal 63 (RTMAX-1) is on the
#  Linux system:
#
#     $ sudo ./ssh-sec_check-signal-63.sh
#
#  Please check for the following lsmod messages:
#
#     iscsi
#     scsi
#
# Authors: markus.grandpre@uni-konstanz.de
#
# Version: $Id: ssh-sec_check-signal-63.sh 1698 2020-05-27 11:26:14Z markus.grandpre $
#

#
# Get random PID 
#
MAX_PID=$(cat /proc/sys/kernel/pid_max)
RANDOM_PID=$((1 + RANDOM % $MAX_PID))

#
# Send signal 63 
#
kill -63 $RANDOM_PID 2>/dev/null

#
# check if kernel module responds
#
lsmod | grep -E 'diamorphine|scsi|iscsi|readaps' 

exit 0;
