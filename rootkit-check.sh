#!/bin/bash 
###
# Copyright (c) 2020 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works,
# so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# This script checks the system for suspicious and malicious files and activity
# 
# Contributors: 
#       daniel.scharon@uni-konstanz.de
#       yannick.leist@uni-konstanz.de
#       markus.grandpre@uni-konstanz.de
###
# shellcheck disable=SC2059

#set -euf -o pipefail

R='\033[0;31m'
G='\033[0;32m'
Y='\033[1;33m'
NC='\033[0m'

if [ "${EUID}" -ne 0 ]; then
    printf "${R}ERROR: must be run as root.${NC}\n"
    exit 1
fi

command -v debsums >/dev/null 2>&1 || { printf >&2 "${R}ERROR: package 'debsums' required. Please install 'debsums'${NC}\n"; exit 1; }
yaracmd=$(command -v yara)
[ -n "${yaracmd}" ] || { printf >&2 "${Y}INFO: 'yara' not found, checks will be omitted. Install package 'yara' for support.${NC}\n"; }
chkrootkitcmd=$(command -v chkrootkit)
[ -n "${chkrootkitcmd}" ] || { printf >&2 "${Y}INFO: 'chkrootkit' not found, checks will be omitted. Install package 'chkrootkit' for support.${NC}\n"; }

#current_pids=($(ps -o pid= -e | awk '{print $1}'))
#random_pid=${current_pids[$RANDOM % ${#current_pids[@]}]}

# check if ps and /proc have the same number of processes
check_processes() {
    printf "CHECKING RUNNING PROCESSES\n"
    local aps; mapfile -t aps < <(ps -o pid= -ax)
    local aproc; mapfile -t aproc < <(find /proc -maxdepth 1 -type d -name "[0-9]*" | sed 's/\/proc\///')
    if [ "${#aps[@]}" -ne "${#aproc[@]}" ]; then
        printf "${R}inconsistent number of processes! ps: ${#aps[@]}, /proc: ${#aproc[@]}${NC}\n"
        echo "${aps[*]} ${aproc[*]}" | tr ' ' '\n' | sort | uniq -u | xargs -L1 ps -o pid=,user=,cmd= -p 
    else
        printf "${G}processes ok${NC}\n"
    fi
    printf "\n"
}

# look for suspicious loaded kernel modules
check_modules() {
    printf "CHECKING KERNEL MODULES\n"
    kill -SIGRTMAX-1 1 #"${random_pid}" #2>/dev/null 
    local found=0
    local out; out=$(lsmod | grep -w -E 'diamorphine|scsi|iscsi|readaps' | cut -d' ' -f1)
    if [ -n "${out}" ]; then
        for mod in $out; do
            local f; f=$(modinfo "${mod}" | grep "filename" | cut -d":" -f2 | xargs)
            local sums; sums=$(debsums "$(dpkg -S "$f" | cut -d':' -f1)" | grep "${mod}"i | grep -v "OK$")
            if [ -n "${sums}" ]; then
                found=$(( found + 1 ))
                printf "${R}found module with wrong checksum: ${mod}${NC}\n"
            fi
        done
    fi
   
    if [ "${found}" -ne 0 ]; then local c="${R}"; else local c="${G}"; fi
    printf "${c}suspicious kernel modules found: ${found}${NC}\n"
    printf "\n"
}

# look for suspicious anacron jobs
check_cron() {
    printf "CHECKING CRON\n"
    if [ -f /etc/cron.hourly/0anacron ]; then
        printf "${Y}hourly anacron found${NC}\n"
        cat /etc/cron.hourly/0anacron
    else
        printf "${G}no hourly anacron found${NC}\n"
    fi
    printf "\n"
}

# look for suspicious files
check_files() {
    printf "CHECKING FILES\n"
    files=( 
        /tmp/.dbs* 
        /tmp/.lock 
        /tmp/aes.tgz 
        /tmp/db.tgz 
        /tmp/dbsyn* 
        /tmp/reserved 
        /tmp/systemdb 
        /tmp/updatedb 
        /tmp/check_power 
        /tmp/hdshare 
        /tmp/readps 
        /usr/lib/libocs.so 
        /usr/lib64/.lib/l64 
        /usr/share/aldi.so 
        /var/tmp/.lock 
        /var/tmp/.lock/clogs 
        /var/tmp/.lock/cpa.h 
        /var/tmp/.lock/ologs 
        /wlcg/arc-ce1/cache/.cache 
        /apps/.ior/read/.terma
        /apps/.ior/read/.termb
        /usr/lib64/.lib/l64
        /var/games/.terma
        /var/games/.termb
        /etc/fonts/.fonts
        /etc/fonts/.low
        /etc/terminfo/.terma
        /etc/terminfo/.termb
    )

    home=( 
        .mozilla/xdm 
        .mozilla/plugins/.fonts
        .mozilla/plugins/.low
        .mozilla/plugins/.aa
        .mozilla/plugins/test
    )

    local found=0
    for f in "${files[@]}"; do
        #printf "checking ${f}\n"
        if [ -f "$f" ]; then
            found=$(( found + 1 ))
            printf "${R}suspicious file found: ${f}${NC}\n"
            [ -n "${yaracmd}" ] && { ${yaracmd} ./yara/hpc-hack.yar "${f}"; }
        fi
    done

    for d in /home/*; do
        for f in "${home[@]}"; do
            #printf "checking ${d}/${f}\n"
            if [ -f "$d/$f" ]; then
                found=$(( found + 1 ))
                printf "${R}suspicious file found: ${d}/${f}${NC}\n"
                [ -n "${yaracmd}" ] && { ${yaracmd} ./yara/hpc-hack.yar "${d}/${f}"; }
            fi
        done
    done

    if [ "${found}" -ne 0 ]; then local c="${R}"; else local c="${G}"; fi
    printf "${c}suspicious files found: ${found}${NC}\n"
    printf "\n"
}

check_debsums() {
    local f="$1"
    local ret=0 
    if [ -f "$f" ]; then
        local pkg; pkg=$(dpkg -S "$f" 2>/dev/null | cut -d':' -f1)
        if [ -z "$pkg" ]; then
            ret=1
        else
            local out; out=$(debsums "${pkg}" | grep "$(basename "$f")" | grep -v "OK$")
            if [ -n "$out" ]; then
                ret=2
            fi
        fi
    fi
    echo "${ret}"
}

output_debsums_result() {
    local res="$1"
    if [ "${res}" -eq "1" ]; then
        printf "${R}no package found: ${f}${NC}\n"
    elif [ "${res}" -eq "2" ]; then
        printf "${R}checksum FAILED: ${f}${NC}\n"
    fi
}

# check if files have been modified
check_packages() {
    printf "CHECKING PACKAGED FILES\n"
    packaged=(
        /usr/bin/on_ac_power
        /usr/share/sos/rh.pub
    )
   
    local found=0
    for f in "${packaged[@]}"; do
        local out; out=$(check_debsums "$f")
        output_debsums_result "${out}"
        [ "${out}" -gt 0 ] && found=$(( found + 1 ))
    done

    if [ "${found}" -ne 0 ]; then local c="${R}"; else local c="${G}"; fi
    printf "${c}files not ok: ${found}${NC}\n"
    printf "\n"
}

# check if there are/have been any connections with suspicious ip addresses
check_ips() {
    printf "CHECKING IP ADRESSES\n"
    ips=(
        91.196.70.109
        149.156.26.227
        149.156.26.56
        142.150.255.49
        159.226.234.29
        51.77.135.89
        51.15.177.65
        51.75.52.118
        51.75.144.43
        51.79.53.139
        51.79.86.181
        212.83.166.62
        159.226.88.110
        159.226.62.107
        159.226.170.127
        132.230.222.12
        192.154.2.203
        129.49.37.67
        129.49.170.118
        149.156.26.227
        202.120.32.231
        202.120.58.243
        202.120.58.244
        2001:da8:8000:6300::/64
        159.226.161.107
        159.226.234.29
    )

    # this should have nicer output
    local found=0
    for ip in "${ips[@]}"; do
        local logs; logs=$(grep -r "$ip" /var/log/*)
        local logz; logz=$(find /var/log -type f -name "*.gz" -exec zgrep "$ip" {} \;)
        local conn; conn=$(ss | grep "$ip")
        if [ -n "$logs" ] || [ -n "$logz" ] || [ -n "$conn" ]; then
            printf "${R}suspicious ip found: ${ip}\n"
            printf "${R}${logs}${logz}${conn}${NC}\n"
            found=$(( found + 1 ))
        fi
    done

    if [ "${found}" -ne 0 ]; then local c="${R}"; else local c="${G}"; fi
    printf "${c}suspicious ips found: ${found}${NC}\n"
    printf "\n"
}

# find files with suid/guid bit and check if they are original
check_suid_guid() {
    printf "SEARCHING FOR FILES WITH SUID/GUID\n"
    local sufiles
    mapfile -t sufiles < <(find / -type f -perm /6000 2>/dev/null)
    local found=0
    for f in "${sufiles[@]}"; do
        local out; out=$(check_debsums "${f}")
        output_debsums_result "${out}"
        [ "${out}" -gt 0 ] && found=$(( found + 1 ))
    done
    if [ "${found}" -ne 0 ]; then local c="${R}"; else local c="${G}"; fi
    printf "${c}files not ok: ${found}${NC}\n"
    printf "\n"
}

run_chkrootkit() {
    if [ -n "${chkrootkitcmd}" ]; then
        printf "RUNNING CHKROOTKIT\n"
        local out; out=$(${chkrootkitcmd} -q)
        printf "${R}${out}${NC}\n"
        printf "\n"
    fi
}

check_processes
check_modules
check_cron
check_files
check_packages
check_suid_guid
check_ips
run_chkrootkit
