typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned int    uint;
typedef unsigned long    ulong;
typedef unsigned char    undefined1;
typedef unsigned int    undefined4;
typedef unsigned long    undefined8;
typedef unsigned short    word;
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef struct stat stat, *Pstat;

typedef ulong __dev_t;

typedef ulong __ino_t;

typedef ulong __nlink_t;

typedef uint __mode_t;

typedef uint __uid_t;

typedef uint __gid_t;

typedef long __off_t;

typedef long __blksize_t;

typedef long __blkcnt_t;

typedef struct timespec timespec, *Ptimespec;

typedef long __time_t;

struct timespec {
    __time_t tv_sec;
    long tv_nsec;
};

struct stat {
    __dev_t st_dev;
    __ino_t st_ino;
    __nlink_t st_nlink;
    __mode_t st_mode;
    __uid_t st_uid;
    __gid_t st_gid;
    int __pad0;
    __dev_t st_rdev;
    __off_t st_size;
    __blksize_t st_blksize;
    __blkcnt_t st_blocks;
    struct timespec st_atim;
    struct timespec st_mtim;
    struct timespec st_ctim;
    long __unused[3];
};

typedef struct tm tm, *Ptm;

struct tm {
    int tm_sec;
    int tm_min;
    int tm_hour;
    int tm_mday;
    int tm_mon;
    int tm_year;
    int tm_wday;
    int tm_yday;
    int tm_isdst;
    long tm_gmtoff;
    char * tm_zone;
};

typedef __time_t time_t;

typedef ulong size_t;

typedef long __ssize_t;

typedef __ssize_t ssize_t;

typedef int __pid_t;

typedef struct passwd passwd, *Ppasswd;

struct passwd {
    char * pw_name;
    char * pw_passwd;
    __uid_t pw_uid;
    __gid_t pw_gid;
    char * pw_gecos;
    char * pw_dir;
    char * pw_shell;
};

typedef struct utimbuf utimbuf, *Putimbuf;

struct utimbuf {
    __time_t actime;
    __time_t modtime;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};




void _DT_INIT(void)

{
  __gmon_start__();
  return;
}



void FUN_00400b30(void)

{
                    // WARNING: Treating indirect jump as call
  (*(code *)(undefined *)0x0)();
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

int utime(char *__file,utimbuf *__file_times)

{
  int iVar1;
  
  iVar1 = utime(__file,__file_times);
  return iVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void free(void *__ptr)

{
  free(__ptr);
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

char * strncpy(char *__dest,char *__src,size_t __n)

{
  char *pcVar1;
  
  pcVar1 = strncpy(__dest,__src,__n);
  return pcVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

int strncmp(char *__s1,char *__s2,size_t __n)

{
  int iVar1;
  
  iVar1 = strncmp(__s1,__s2,__n);
  return iVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

char * strcpy(char *__dest,char *__src)

{
  char *pcVar1;
  
  pcVar1 = strcpy(__dest,__src);
  return pcVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

int fcntl(int __fd,int __cmd,...)

{
  int iVar1;
  
  iVar1 = fcntl(__fd,__cmd);
  return iVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

ssize_t write(int __fd,void *__buf,size_t __n)

{
  ssize_t sVar1;
  
  sVar1 = write(__fd,__buf,__n);
  return sVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

__pid_t getpid(void)

{
  __pid_t _Var1;
  
  _Var1 = getpid();
  return _Var1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

char * ctime(time_t *__timer)

{
  char *pcVar1;
  
  pcVar1 = ctime(__timer);
  return pcVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

size_t strlen(char *__s)

{
  size_t sVar1;
  
  sVar1 = strlen(__s);
  return sVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

char * strchr(char *__s,int __c)

{
  char *pcVar1;
  
  pcVar1 = strchr(__s,__c);
  return pcVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

int printf(char *__format,...)

{
  int iVar1;
  
  iVar1 = printf(__format);
  return iVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

int ftruncate(int __fd,__off_t __length)

{
  int iVar1;
  
  iVar1 = ftruncate(__fd,__length);
  return iVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

__off_t lseek(int __fd,__off_t __offset,int __whence)

{
  __off_t _Var1;
  
  _Var1 = lseek(__fd,__offset,__whence);
  return _Var1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void * memset(void *__s,int __c,size_t __n)

{
  void *pvVar1;
  
  pvVar1 = memset(__s,__c,__n);
  return pvVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

int close(int __fd)

{
  int iVar1;
  
  iVar1 = close(__fd);
  return iVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

ssize_t read(int __fd,void *__buf,size_t __nbytes)

{
  ssize_t sVar1;
  
  sVar1 = read(__fd,__buf,__nbytes);
  return sVar1;
}



void __libc_start_main(void)

{
  __libc_start_main();
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void * calloc(size_t __nmemb,size_t __size)

{
  void *pvVar1;
  
  pvVar1 = calloc(__nmemb,__size);
  return pvVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

int strcmp(char *__s1,char *__s2)

{
  int iVar1;
  
  iVar1 = strcmp(__s1,__s2);
  return iVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

passwd * getpwnam(char *__name)

{
  passwd *ppVar1;
  
  ppVar1 = getpwnam(__name);
  return ppVar1;
}



void __gmon_start__(void)

{
  __gmon_start__();
  return;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void * memcpy(void *__dest,void *__src,size_t __n)

{
  void *pvVar1;
  
  pvVar1 = memcpy(__dest,__src,__n);
  return pvVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

time_t time(time_t *__timer)

{
  time_t tVar1;
  
  tVar1 = time(__timer);
  return tVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

time_t mktime(tm *__tp)

{
  time_t tVar1;
  
  tVar1 = mktime(__tp);
  return tVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

int chown(char *__file,__uid_t __owner,__gid_t __group)

{
  int iVar1;
  
  iVar1 = chown(__file,__owner,__group);
  return iVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

int open(char *__file,int __oflag,...)

{
  int iVar1;
  
  iVar1 = open(__file,__oflag);
  return iVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

int getopt(int ___argc,char **___argv,char *__shortopts)

{
  int iVar1;
  
  iVar1 = getopt(___argc,___argv,__shortopts);
  return iVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

int atoi(char *__nptr)

{
  int iVar1;
  
  iVar1 = atoi(__nptr);
  return iVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

int sprintf(char *__s,char *__format,...)

{
  int iVar1;
  
  iVar1 = sprintf(__s,__format);
  return iVar1;
}



// WARNING: Unknown calling convention yet parameter storage is locked

void exit(int __status)

{
                    // WARNING: Subroutine does not return
  exit(__status);
}



// WARNING: Unknown calling convention yet parameter storage is locked

char * strstr(char *__haystack,char *__needle)

{
  char *pcVar1;
  
  pcVar1 = strstr(__haystack,__needle);
  return pcVar1;
}



void entry(undefined8 param_1,undefined8 param_2,undefined8 param_3)

{
  undefined8 in_stack_00000000;
  undefined auStack8 [8];
  
  __libc_start_main(main,in_stack_00000000,&stack0x00000008,FUN_004027d0,FUN_00402840,param_3,
                    auStack8);
  do {
                    // WARNING: Do nothing block with infinite loop
  } while( true );
}



// WARNING: Removing unreachable block (ram,0x00400d97)
// WARNING: Removing unreachable block (ram,0x00400da1)

void FUN_00400d80(void)

{
  return;
}



void _FINI_0(void)

{
  if (DAT_00603310 == '\0') {
    FUN_00400d80();
    DAT_00603310 = '\x01';
  }
  return;
}



// WARNING: Removing unreachable block (ram,0x00400dd4)
// WARNING: Removing unreachable block (ram,0x00400e24)
// WARNING: Removing unreachable block (ram,0x00400dde)
// WARNING: Globals starting with '_' overlap smaller symbols at the same address

void _INIT_0(void)

{
  return;
}



void scramble(char *p,int count)

{
  int local_10;
  int i;
  
  if (0 < count) {
    local_10 = count * 0x8249;
    i = 0;
    while (local_10 = (local_10 + 0x39ef) % 0x52c7, i < count) {
      p[i] = (byte)local_10 ^ p[i];
      local_10 = local_10 * 0x8249;
      i = i + 1;
    }
  }
  return;
}



void printmessage(void)

{
  printf(&ERRORARGSEXIT);
  return;
}



// WARNING: Could not reconcile some variable overlaps

ulong main(int argc,char **argv)

{
  char *__s1;
  char *pcVar1;
  bool opbh;
  bool optw;
  bool optb;
  bool optl;
  bool optm;
  bool opts;
  bool opta;
  int numberarg;
  char uitistgleich [40];
  passwd *password;
  char *local_68;
  char opt;
  uint local_18;
  uint retval;
  char *filename;
  
  scramble(&UTMP,0xd);
  scramble(&WTMP,0xd);
  scramble(&BTMP,0xd);
  scramble(&LASTLOG,0x10);
  scramble(&MESSAGES,0x11);
  scramble(&SECURE,0xf);
  scramble(&WARN,0xd);
  scramble(&DEBUG,0xe);
  scramble(&AUDIT0,0x18);
  scramble(&AUDIT1,0x1a);
  scramble(&AUDIT2,0x1a);
  scramble(&AUTHLOG,0x11);
  scramble(&HISTORY,0x1b);
  scramble(&AUTHPRIV,0x11);
  scramble(&DEAMONLOG,0x13);
  scramble(&SYSLOG,0xf);
  scramble(&ACHTdPROZENTs,7);
  scramble(&OPTOPTS,0xb);
  scramble(&UIDISPROZD,7);
  scramble(&ERRORARGSEXIT,0x11);
  scramble(&ROOT,4);
  filename = (char *)0x0;
  local_18 = 0;
  opbh = false;
  optw = false;
  optb = false;
  optl = false;
  optm = false;
  opts = false;
  opta = false;
  now = time((time_t *)0x0);
  while (_opt = getopt(argc,argv,&OPTOPTS), _opt != -1) {
    switch(_opt) {
    case 0x61:
      opta = true;
      break;
    case 0x62:
      optb = true;
      break;
    default:
      printmessage();
                    // WARNING: Subroutine does not return
      exit(1);
    case 0x66:
      filename = optarg;
      break;
    case 0x68:
      opbh = true;
      break;
    case 0x6c:
      optl = true;
      break;
    case 0x6d:
      optm = true;
      break;
    case 0x73:
      opts = true;
      break;
    case 0x74:
      local_18 = 1;
      numberarg = atoi(optarg);
      if (numberarg != 0) {
        numberarg = atoi(optarg);
        now = (time_t)numberarg;
        if ((0 < now) && (now < 0x834)) {
          now = settime();
        }
      }
      break;
    case 0x77:
      optw = true;
    }
  }
  if (((((!opbh) && (!optw)) && (!optb)) && ((!optl && (!optm)))) && ((!opts && (!opta)))) {
    printmessage();
  }
  if (opbh) {
    if (argc <= optind + 1) {
      printmessage();
                    // WARNING: Subroutine does not return
      exit(1);
    }
    if (filename == (char *)0x0) {
      filename = &UTMP;
    }
    retval = machmitfile(filename,argv[optind],argv[(long)optind + 1],(ulong)local_18);
  }
  else {
    if (optw) {
      if (argc <= optind + 1) {
        printmessage();
                    // WARNING: Subroutine does not return
        exit(1);
      }
      if (filename == (char *)0x0) {
        filename = &WTMP;
      }
      retval = machmitfile(filename,argv[optind],argv[(long)optind + 1],(ulong)local_18);
    }
    else {
      if (optb) {
        if (argc <= optind + 1) {
          printmessage();
                    // WARNING: Subroutine does not return
          exit(1);
        }
        if (filename == (char *)0x0) {
          filename = &BTMP;
        }
        retval = machmitfile(filename,argv[optind],argv[(long)optind + 1],(ulong)local_18);
      }
      else {
        if (optl) {
          if (argc <= optind) {
            printmessage();
                    // WARNING: Subroutine does not return
            exit(1);
          }
          if (filename == (char *)0x0) {
            filename = &LASTLOG;
          }
          retval = writezerosinfile(filename,argv[optind],argv[optind]);
        }
        else {
          if (optm) {
            if (argc <= optind + 3) {
              printmessage();
                    // WARNING: Subroutine does not return
              exit(1);
            }
            if (filename == (char *)0x0) {
              filename = &LASTLOG;
            }
            retval = FUN_00401bb0(filename,argv[optind],argv[(long)optind + 1],
                                  argv[(long)optind + 2],argv[(long)optind + 3]);
          }
          else {
            if (opts) {
              if (argc <= optind) {
                printmessage();
                    // WARNING: Subroutine does not return
                exit(1);
              }
              local_68 = argv[optind];
              if (filename == (char *)0x0) {
                printmessage();
              }
              else {
                retval = machshitmitfile(filename,local_68,(ulong)local_18,local_68);
              }
            }
            else {
              if (opta) {
                if (argc <= optind + 1) {
                  printmessage();
                    // WARNING: Subroutine does not return
                  exit(1);
                }
                __s1 = argv[optind];
                pcVar1 = argv[(long)optind + 1];
                numberarg = strcmp(__s1,&ROOT);
                if (numberarg == 0) {
                  local_18 = 1;
                }
                machmitfile(&WTMP,__s1,pcVar1,(ulong)local_18);
                machmitfile(&UTMP,__s1,pcVar1,(ulong)local_18);
                machmitfile(&BTMP,__s1,pcVar1,(ulong)local_18);
                writezerosinfile(&LASTLOG,__s1,__s1);
                machshitmitfile(&MESSAGES,__s1,(ulong)local_18,__s1);
                machshitmitfile(&MESSAGES,pcVar1,(ulong)local_18,pcVar1);
                machshitmitfile(&SECURE,__s1,(ulong)local_18,__s1);
                machshitmitfile(&SECURE,pcVar1,(ulong)local_18,pcVar1);
                machshitmitfile(&AUTHPRIV,__s1,(ulong)local_18,__s1);
                machshitmitfile(&AUTHPRIV,pcVar1,(ulong)local_18,pcVar1);
                machshitmitfile(&DEAMONLOG,__s1,(ulong)local_18,__s1);
                machshitmitfile(&DEAMONLOG,pcVar1,(ulong)local_18,pcVar1);
                machshitmitfile(&SYSLOG,__s1,(ulong)local_18,__s1);
                machshitmitfile(&SYSLOG,pcVar1,(ulong)local_18,pcVar1);
                machshitmitfile(&WARN,__s1,(ulong)local_18,__s1);
                machshitmitfile(&WARN,pcVar1,(ulong)local_18,pcVar1);
                machshitmitfile(&DEBUG,__s1,(ulong)local_18,__s1);
                machshitmitfile(&DEBUG,pcVar1,(ulong)local_18,pcVar1);
                machshitmitfile(&AUDIT0,__s1,(ulong)local_18,__s1);
                machshitmitfile(&AUDIT0,pcVar1,(ulong)local_18,pcVar1);
                machshitmitfile(&AUDIT1,__s1,(ulong)local_18,__s1);
                machshitmitfile(&AUDIT1,pcVar1,(ulong)local_18,pcVar1);
                machshitmitfile(&AUDIT2,__s1,(ulong)local_18,__s1);
                machshitmitfile(&AUDIT2,pcVar1,(ulong)local_18,pcVar1);
                machshitmitfile(&AUTHLOG,__s1,(ulong)local_18,__s1);
                machshitmitfile(&AUTHLOG,pcVar1,(ulong)local_18,pcVar1);
                machshitmitfile(&HISTORY,__s1,(ulong)local_18,__s1);
                retval = machshitmitfile(&HISTORY,pcVar1,(ulong)local_18,pcVar1);
                password = getpwnam(__s1);
                if (password != (passwd *)0x0) {
                  sprintf(uitistgleich,&UIDISPROZD,(ulong)password->pw_uid);
                  machshitmitfile(&SECURE,uitistgleich,(ulong)local_18,uitistgleich);
                  machshitmitfile(&AUDIT0,uitistgleich,(ulong)local_18,uitistgleich);
                  machshitmitfile(&AUDIT1,uitistgleich,(ulong)local_18,uitistgleich);
                  retval = machshitmitfile(&AUDIT2,uitistgleich,(ulong)local_18,uitistgleich);
                }
              }
            }
          }
        }
      }
    }
  }
  return (ulong)retval;
}



// WARNING: Globals starting with '_' overlap smaller symbols at the same address

ulong writezerosinfile(char *filename,char *username)

{
  __off_t _Var1;
  ssize_t sVar2;
  undefined buffer [300];
  int fh;
  passwd *passedstruct;
  uint local_c;
  
  memset(buffer,0,0x124);
  local_c = FUN_00401dc8(filename,&DAT_00603440);
  if (((-1 < (int)local_c) && (passedstruct = getpwnam(username), passedstruct != (passwd *)0x0)) &&
     (fh = open(filename,2), 0 < fh)) {
    _DAT_00603320 = 1;
    _DAT_00603328 = 0;
    _DAT_00603322 = 0;
    _DAT_00603330 = 0;
    _DAT_00603338 = getpid();
    local_c = fcntl(fh,7,&DAT_00603320);
    if (local_c != 0xffffffff) {
      _Var1 = lseek(fh,(ulong)passedstruct->pw_uid * 0x124,0);
      local_c = (uint)_Var1;
      sVar2 = write(fh,buffer,0x124);
      local_c = (uint)sVar2;
      local_c = close(fh);
      local_c = FUN_00401dfa(filename,&DAT_00603440);
    }
  }
  printf(&ACHTdPROZENTs,(ulong)(local_c + 1),filename,(ulong)(local_c + 1));
  return (ulong)local_c;
}



// WARNING: Globals starting with '_' overlap smaller symbols at the same address

ulong FUN_00401bb0(char *param_1,char *param_2,char *param_3,char *param_4)

{
  size_t __n;
  __off_t _Var1;
  ssize_t sVar2;
  undefined4 local_148;
  char acStack324 [32];
  char acStack292 [264];
  int local_1c;
  passwd *local_18;
  uint local_c;
  
  memset(&local_148,0,0x124);
  local_c = FUN_00401dc8(param_1,&DAT_00603440);
  if (((-1 < (int)local_c) && (local_18 = getpwnam(param_2), local_18 != (passwd *)0x0)) &&
     (local_1c = open(param_1,2), 0 < local_1c)) {
    _DAT_00603340 = 1;
    _DAT_00603348 = 0;
    _DAT_00603342 = 0;
    _DAT_00603350 = 0;
    _DAT_00603358 = getpid();
    local_c = fcntl(local_1c,7,&DAT_00603340);
    if (local_c != 0xffffffff) {
      __n = strlen(param_4);
      strncpy(acStack324,param_4,__n);
      __n = strlen(param_3);
      strncpy(acStack292,param_3,__n);
      local_148 = settime();
      _Var1 = lseek(local_1c,(ulong)local_18->pw_uid * 0x124,0);
      local_c = (uint)_Var1;
      sVar2 = write(local_1c,&local_148,0x124);
      local_c = (uint)sVar2;
      local_c = close(local_1c);
      local_c = FUN_00401dfa(param_1,&DAT_00603440);
    }
  }
  printf(&ACHTdPROZENTs,(ulong)(local_c + 1),param_1,(ulong)(local_c + 1));
  return (ulong)local_c;
}



ulong FUN_00401dc8(undefined8 param_1,undefined8 param_2)

{
  uint uVar1;
  
  uVar1 = FUN_00402850(param_1,param_2,param_2);
  return (ulong)uVar1;
}



ulong FUN_00401dfa(char *param_1,long param_2)

{
  utimbuf local_28;
  __gid_t local_14;
  __uid_t local_10;
  uint local_c;
  
  local_28.actime = *(__time_t *)(param_2 + 0x48);
  local_28.modtime = *(__time_t *)(param_2 + 0x58);
  local_10 = *(__uid_t *)(param_2 + 0x1c);
  local_14 = *(__gid_t *)(param_2 + 0x20);
  local_c = chown(param_1,local_10,local_14);
  if (-1 < (int)local_c) {
    local_c = utime(param_1,&local_28);
  }
  return (ulong)local_c;
}



void settime(char *param_1)

{
  char *pcVar1;
  tm local_48;
  
  memset(&local_48,0,0x38);
  local_48.tm_isdst = 1;
  if (param_1 != (char *)0x0) {
    local_48.tm_year = atoi(param_1);
    local_48.tm_year = local_48.tm_year + -0x76c;
    pcVar1 = strchr(param_1,0x2d);
    if (pcVar1 != (char *)0x0) {
      local_48.tm_mon = atoi(pcVar1 + 1);
      local_48.tm_mon = local_48.tm_mon + -1;
      pcVar1 = strchr(pcVar1 + 1,0x2d);
      if (pcVar1 != (char *)0x0) {
        local_48.tm_mday = atoi(pcVar1 + 1);
        pcVar1 = strchr(pcVar1 + 1,0x2d);
        if (pcVar1 != (char *)0x0) {
          local_48.tm_hour = atoi(pcVar1 + 1);
          pcVar1 = strchr(pcVar1 + 1,0x3a);
          if (pcVar1 != (char *)0x0) {
            local_48.tm_min = atoi(pcVar1 + 1);
            pcVar1 = strchr(pcVar1 + 1,0x3a);
            if (pcVar1 != (char *)0x0) {
              local_48.tm_sec = atoi(pcVar1 + 1);
            }
          }
        }
      }
    }
  }
  mktime(&local_48);
  return;
}



// WARNING: Globals starting with '_' overlap smaller symbols at the same address

ulong machshitmitfile(char *param_1,char *param_2,int param_3)

{
  __off_t _Var1;
  ssize_t readfromfh;
  char *__src;
  char local_198 [128];
  char local_118 [4];
  char local_114 [12];
  undefined local_108;
  time_t local_90;
  int local_84;
  char *local_80;
  char *local_78;
  uint local_6c;
  char *local_68;
  int local_5c;
  char *local_58;
  int local_50;
  int fh;
  int local_48;
  int local_44;
  uint local_40;
  int local_3c;
  char *local_38;
  uint local_2c;
  int local_28;
  int local_24;
  char *local_20;
  int local_18;
  int local_14;
  uint local_10;
  int local_c;
  
  local_10 = FUN_00401dc8(param_1,&DAT_00603440);
  if ((-1 < (int)local_10) && (fh = open(param_1,2), 0 < fh)) {
    _DAT_00603360 = 1;
    _DAT_00603368 = 0;
    _DAT_00603362 = 0;
    _DAT_00603370 = 0;
    _pid = getpid();
    local_10 = fcntl(fh,7,&DAT_00603360);
    if (local_10 != 0xffffffff) {
      _Var1 = lseek(fh,0,2);
      local_50 = (int)_Var1;
      local_58 = (char *)calloc((long)(local_50 + 1),1);
      lseek(fh,0,0);
      readfromfh = read(fh,local_58,(long)local_50);
      local_5c = (int)readfromfh;
      if (local_5c == local_50) {
        local_14 = -1;
        local_18 = 0;
        local_c = 0;
        while (local_c < local_50) {
          if (local_58[local_c] == '\0') {
            if (local_14 == -1) {
              local_14 = local_c;
            }
          }
          else {
            local_58[local_18] = local_58[local_c];
            local_18 = local_18 + 1;
          }
          local_c = local_c + 1;
        }
        local_58[local_18] = '\0';
        if (local_14 == -1) {
          local_14 = local_18;
        }
        local_20 = local_58;
        local_24 = 0;
        local_28 = -1;
        local_2c = 0;
        while (local_24 != 1) {
          local_38 = local_20;
          while ((*local_38 != '\n' && (*local_38 != '\0'))) {
            local_38 = local_38 + 1;
          }
          if (*local_38 == '\0') {
            local_24 = 1;
          }
          *local_38 = '\0';
          local_68 = strstr(local_20,param_2);
          local_6c = (uint)(local_68 != (char *)0x0);
          local_3c = 0;
          if ((local_68 != (char *)0x0) && (param_3 != 0)) {
            local_c = 0;
            while (local_c < 3) {
              sprintf(local_198,"%ld",now / 100 - (long)local_c);
              local_78 = strstr(local_20,local_198);
              if (local_78 != (char *)0x0) {
                local_3c = 1;
                break;
              }
              local_c = local_c + 1;
            }
            if (local_3c == 0) {
              local_c = 0;
              while (local_c < 5) {
                local_90 = now - local_c * 0x3c;
                __src = ctime(&local_90);
                strcpy(local_118,__src);
                local_108 = 0;
                local_80 = strstr(local_20,local_114);
                if (local_80 != (char *)0x0) {
                  local_3c = 1;
                  break;
                }
                local_c = local_c + 1;
              }
            }
          }
          local_40 = local_6c;
          if ((param_3 != 0) && (local_3c == 0)) {
            local_40 = 0;
          }
          if (local_40 == 0) {
            if (local_24 == 0) {
              *local_38 = '\n';
            }
          }
          else {
            local_2c = local_2c + 1;
            if (local_28 == -1) {
              local_28 = (int)local_20 - (int)local_58;
            }
            memset(local_20,0,(size_t)(local_38 + (1 - (long)local_20)));
          }
          local_20 = local_38 + 1;
        }
        if (local_28 == -1) {
          local_28 = local_18;
        }
        local_44 = local_28;
        local_c = local_28;
        while (local_c < local_18) {
          if (local_58[local_c] != '\0') {
            local_58[local_44] = local_58[local_c];
            local_44 = local_44 + 1;
          }
          local_c = local_c + 1;
        }
        if (local_44 != local_50) {
          local_48 = local_28;
          if (local_14 < local_28) {
            local_48 = local_14;
          }
          lseek(fh,(long)local_48,0);
          local_84 = local_44 - local_48;
          write(fh,local_58 + local_48,(long)local_84);
          ftruncate(fh,(long)local_44);
        }
        free(local_58);
        close(fh);
        FUN_00401dfa(param_1,&DAT_00603440);
      }
      else {
        close(fh);
      }
    }
  }
  printf(&ACHTdPROZENTs,(ulong)local_2c,param_1);
  return (ulong)local_10;
}



// WARNING: Globals starting with '_' overlap smaller symbols at the same address

ulong machmitfile(char *filename,char *param_2,char *param_3,int param_4)

{
  int __fd;
  int iVar1;
  __off_t _Var2;
  void *__buf;
  ssize_t flength;
  uint local_24;
  void *bufpointer;
  void *c;
  uint local_c;
  
  local_c = FUN_00401dc8(filename,&DAT_00603440);
  if ((-1 < (int)local_c) && (__fd = open(filename,2), 0 < __fd)) {
    _DAT_00603380 = 1;
    _DAT_00603388 = 0;
    _DAT_00603382 = 0;
    _DAT_00603390 = 0;
    _DAT_00603398 = getpid();
    local_c = fcntl(__fd,7,&DAT_00603380);
    if (local_c != 0xffffffff) {
      _Var2 = lseek(__fd,0,2);
      iVar1 = (int)_Var2;
      __buf = calloc((long)(iVar1 + 1),1);
      lseek(__fd,0,0);
      flength = read(__fd,__buf,(long)iVar1);
      if ((int)flength == iVar1) {
        local_24 = 0;
        bufpointer = __buf;
        c = __buf;
        while (c < (void *)((long)__buf + (long)(int)flength)) {
          if ((*(int *)((long)c + 0x154) == 0) ||
             (((iVar1 = strncmp((char *)((long)c + 0x4c),param_3,0x100), iVar1 == 0 &&
               (iVar1 = strncmp((char *)((long)c + 0x2c),param_2,0x20), iVar1 == 0)) &&
              ((param_4 == 0 ||
               (((param_4 != 0 && (now + -300 <= (long)*(int *)((long)c + 0x154))) &&
                (*(int *)((long)c + 0x154) <= now)))))))) {
            local_24 = local_24 + 1;
          }
          else {
            memcpy(bufpointer,c,0x180);
            bufpointer = (void *)((long)bufpointer + 0x180);
          }
          c = (void *)((long)c + 0x180);
        }
        iVar1 = (int)bufpointer - (int)__buf;
        lseek(__fd,0,0);
        write(__fd,__buf,(long)iVar1);
        ftruncate(__fd,(long)iVar1);
        free(__buf);
        close(__fd);
        FUN_00401dfa(filename,&DAT_00603440);
      }
      else {
        close(__fd);
      }
    }
  }
  printf(&ACHTdPROZENTs,(ulong)local_24,filename);
  return (ulong)local_c;
}



void FUN_004027d0(ulong param_1,undefined8 param_2,undefined8 param_3)

{
  long lVar1;
  
  lVar1 = 0;
  _DT_INIT();
  do {
    (*(code *)(&__DT_INIT_ARRAY)[lVar1])(param_1 & 0xffffffff,param_2,param_3);
    lVar1 = lVar1 + 1;
  } while (lVar1 != 1);
  return;
}



void FUN_00402840(void)

{
  return;
}



void FUN_00402850(char *param_1,stat *param_2)

{
  __xstat(1,param_1,param_2);
  return;
}



void _DT_FINI(void)

{
  return;
}


