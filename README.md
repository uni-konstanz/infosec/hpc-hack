# hpc-hack

Ressources related to the HPC hack.

**usage:**

    $ git clone https://gitlab.inf.uni-konstanz.de/infosec/hpc-hack.git
    $ cd hpc-hack
    $ sudo ./rootkit-check.sh

**Ideal output:**

    CHECKING RUNNING PROCESSES
    processes ok

    CHECKING KERNEL MODULES
    suspicious kernel modules found: 0

    CHECKING CRON
    no hourly anacron found

    CHECKING FILES
    suspicious files found: 0

    CHECKING PACKAGED FILES
    files not ok: 0

    SEARCHING FOR FILES WITH SUID/SUID
    files not ok: 0

    CHECKING IP ADRESSES
    suspicious ips found: 0

**Dependencies:**

`rootkit-check.sh` uses the following packages:

- `debsums`: required. Checks the checksum of packaged files
- `yara`: optional. Searches for files with known malware checksums
- `chkrootkit`: optional. Performs various rootkit tests

## Links

- EGI Report: <https://csirt.egi.eu/academic-data-centers-abused-for-crypto-currency-mining/>
- Analysis of incidents: <https://www.cadosecurity.com/2020/05/16/1318/>
- Analysis of binaries: <https://atdotde.blogspot.com/2020/05/high-performance-hackers.html>
- Tillmann Security Researcher: <https://twitter.com/nunohaien>
- Indicators: <https://otx.alienvault.com/pulse/5ebff3b3a78ca09f7191f79f>
- Yara: <https://github.com/VirusTotal/yara>
- Rootkit: <https://github.com/m0nad/Diamorphine>
- Logtamper (cleaner): <https://github.com/border/wifihack/tree/master/bin/logtamper>
- Module Info: <https://www.cyberciti.biz/tips/how-to-display-or-show-information-about-a-linux-kernel-module-or-drivers.html>
