rule loader { 
    meta:
        description = "Indicator for the loader .fonts"
        author = "Tillmann Werner"
        reference = "https://twitter.com/nunohaien"
    strings: 
        $ = { 61 31 C2 8B 45 FC 48 98 }
    condition:
        all of them

}

rule cleaner {
    meta:
        description = "Indicator for the log cleaner .low"
        author = "Tillmann Werner"
        reference = "https://twitter.com/nunohaien"
    strings:
        $ = { 14 CC FC 28 25 DE B9 }
    condition:
        all of them
}
